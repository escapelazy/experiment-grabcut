# Feature matching
# Developer: Minwoo Kim (Rabbit Cave King)
# E-mail: escapelazy@gmail.com
# Copyright 2019. Minwoo Kim (Rabbit Cave King)

# Import modules
import cv2 as cv
import matplotlib.pyplot as plt

# Read original / experiment image
oriImg = cv.imread('./Image/Original/expImg_3.jpg')
expImg = cv.imread('./Image/GrabCut/grabcutImg_3_result.png')

# cv2.ORB_create(int nfeatures = N, ...)
# nfeatures = The maximum number of features to retain.
detector = cv.ORB_create()
keyPoints0, feature0 = detector.detectAndCompute(oriImg, None)
keyPoints1, feature1 = detector.detectAndCompute(expImg, None)

matcher = cv.BFMatcher_create(cv.NORM_HAMMING, False)
matches01 = matcher.knnMatch(feature0, feature1, k = 2)
matches10 = matcher.knnMatch(feature1, feature0, k = 2)

def ratioTest (matches, threshold):
    bestMatches = []
    for m in matches:
        ratio = m[0].distance / m[1].distance
        if ratio < threshold:
            bestMatches.append(m[0])
    return bestMatches

RATIO_THRESHOLD = 0.7

bestMatches01 = ratioTest(matches01, RATIO_THRESHOLD)
bestMatches10 = ratioTest(matches10, RATIO_THRESHOLD)
_bestMatches10 = {(m.trainIdx, m.queryIdx) for m in bestMatches10}

final = [m for m in bestMatches01 if (m.queryIdx, m.trainIdx) in _bestMatches10]

resImg = cv.drawMatches(oriImg, keyPoints0, expImg, keyPoints1, final, None)

plt.figure()
plt.imshow(resImg[:, :, [2, 1, 0]])
plt.tight_layout()
plt.show()